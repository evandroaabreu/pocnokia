package pocIsoladores;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.mockito.internal.matchers.Any;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import com.nokia.poc.pocIsoladores.entity.InspecaoResultado;
import com.nokia.poc.pocIsoladores.repository.InspecaoRepository;
import com.nokia.poc.pocIsoladores.service.AwsS3Service;
import com.nokia.poc.pocIsoladores.service.InspecaoService;
import com.nokia.poc.pocIsoladores.service.OrquestradorService;

@RunWith(SpringRunner.class)
public class InspecaoTest {
	
	
	@InjectMocks
	InspecaoService inspecaoService;

	@Mock
	InspecaoRepository inspecaoRepository;
	
	@Mock
	AwsS3Service awsS3Service;
	
	@Mock
	OrquestradorService orqService;
	
	@Test
	public void salvaInspecaoOk() throws Exception {
		InspecaoResultado inspecaoResultado = new InspecaoResultado();
		inspecaoResultado.setId(1);
		when(awsS3Service.uploadFile(Mockito.any(File.class))).thenReturn("url");
		when(orqService.inferenceMlFile(Mockito.any(File.class), Mockito.any(Integer.class))).thenReturn(new ArrayList<String>());
		when(inspecaoRepository.save(Mockito.any(InspecaoResultado.class))).thenReturn(new InspecaoResultado());
		assertEquals(0, inspecaoService.salvaInspecao(getFileInspecao(), -10.00, -15.00).getTypeResponse());
	}	
	
	
	@Test
	public void getPorIdNaoEncontrado() {
		assertEquals(3, inspecaoService.getPorId(1).getResponse().getTypeResponse());
	}	
	
	@Test
	public void getPorIdOK() throws IllegalStateException, IOException {
		InspecaoResultado inspecaoResultado = builderInspecaoResultado();
		Optional<InspecaoResultado> opInspecao = Optional.of(inspecaoResultado);
		when(inspecaoRepository.findById(1)).thenReturn(opInspecao);		
		assertEquals(0, inspecaoService.getPorId(1).getResponse().getTypeResponse());

	}

	private MockMultipartFile getFileInspecao() {
		
		File fileImagem = new File("src/test/resources/imagens/isolador.jpg");
	    InputStream streamImagem;
		try {
			streamImagem = new FileInputStream(fileImagem);
			MockMultipartFile imagem = new MockMultipartFile("img", streamImagem);
			
			return imagem;
			
		} catch (IOException e) {
		}
		return null;
	}
	
	
	@Test
	public void apagaPorIdOK() throws IllegalStateException, IOException {
		InspecaoResultado inspecaoResultado = builderInspecaoResultado();
		Optional<InspecaoResultado> opInspecao = Optional.of(inspecaoResultado);
		when(inspecaoRepository.findById(1)).thenReturn(opInspecao);
		assertEquals(0, inspecaoService.apagaPorId(1).getTypeResponse());
	}
	
	@Test
	public void apagaPorIdNaoEncontrado() throws IllegalStateException, IOException {
		assertEquals(3, inspecaoService.apagaPorId(1).getTypeResponse());
	}
		
	

	private InspecaoResultado builderInspecaoResultado() throws IllegalStateException, IOException {
		return InspecaoResultado.builder()
				.id(1)
				.imagem(getFile())
				.latitude(-10.00).longitude(-20.00).build();
	}
	
	
	private byte[] getFile() throws IllegalStateException, IOException {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew);
		getFileInspecao().transferTo(convFile);
		byte[] imagemBytes = Files.readAllBytes(convFile.toPath());
		return imagemBytes;
	}
	

}
