package com.nokia.poc.pocIsoladores.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

public class Util {

	public static Response buildResponse(int typeResponse, String message) {
		return Response.builder().typeResponse(typeResponse).message(message).build();
	}
	
	public static File convertMultiPartFileToFile(MultipartFile imagem) throws Exception  {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew);
		try {
			imagem.transferTo(convFile);
		} catch (IllegalStateException | IOException e) {
			throw new Exception(e);
		}
		return convFile;		
	}
	
	public static File convertByteArrayToFileXls(byte[] bytes) throws Exception  {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew + ".xls");
		Files.write(Paths.get(convFile.getAbsolutePath()), bytes);
		return convFile;
	}

	
	public static File convertByteArrayToFileImg(byte[] bytes) throws Exception  {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew + ".jpg");
		Files.write(Paths.get(convFile.getAbsolutePath()), bytes);
		return convFile;
	}
	
	public static String convertFileToBase64(File file) throws IOException {		
		byte[] imagemBytes = Files.readAllBytes(file.toPath());
		return  Base64.getEncoder().encodeToString(imagemBytes);
	}
	
	public static File convertImgToFile(InputStream initialStream) throws IOException {
		if (initialStream != null) {
			byte[] buffer = new byte[initialStream.available()];
			int read = initialStream.read(buffer);
			if (read > 0) {
				Date date = new Date();
				File targetFile = new File(
						System.getProperty("java.io.tmpdir") + File.separator + date.getTime() + ".jpg");
				try (OutputStream outStream = new FileOutputStream(targetFile)) {
					outStream.write(buffer);
					BufferedImage image;
					image = ImageIO.read(targetFile);
					BufferedImage resized = resize(image, image.getHeight(), image.getWidth());
					File output = new File(
							System.getProperty("java.io.tmpdir") + File.separator + date.getTime() + "_Resize.jpg");
					ImageIO.write(resized, "jpg", output);
					return output;
				}
			} else {
				throw new FileNotFoundException();
			}
		} else {
			throw new FileNotFoundException();
		}
	}
	
	
	
	private static BufferedImage resize(BufferedImage src, int height, int width) {

		int targetWidth = width;
		int targetHeight = height;
		BufferedImage bi = new BufferedImage(targetWidth, targetHeight,
				src.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB
						: BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bi.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR); // produces																									// quality)
		g2d.drawImage(src, 0, 0, targetWidth, targetHeight, null);
		g2d.dispose();
		return bi;
	}
	
	public static String convertArrayToStringJson(List<String> array) {
		StringBuilder stringJson = new StringBuilder();
		stringJson.append("[");
		for (String string : array) {
			stringJson.append("\""+string+"\",");
		}
		stringJson.deleteCharAt(stringJson.length()-1);
		stringJson.append("]");
		return stringJson.toString();
	}
	
}
