package com.nokia.poc.pocIsoladores.enumeration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ClasseComponenteEnum {
	
	FERRAGEM("Ferragem"),
	VIDRO("Vidro"),
	POLIMERICO("Polimerico");
	
	String classe;

	public String getClasse() {
		return classe;
	}
	
}
