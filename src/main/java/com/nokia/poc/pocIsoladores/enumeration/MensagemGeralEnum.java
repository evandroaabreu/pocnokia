package com.nokia.poc.pocIsoladores.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MensagemGeralEnum {
	CONSULTADOCOMSUCESSO(0, ""),
	EMAILENVIADOCOMSUCESSO(0, ""),
	EMAILCOMFALHA(3, "Problema no envio do e-mail!");

	


	private Integer value;
	private String description;

}
