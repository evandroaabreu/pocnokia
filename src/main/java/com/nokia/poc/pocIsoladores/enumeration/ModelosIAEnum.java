package com.nokia.poc.pocIsoladores.enumeration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ModelosIAEnum {
	
	CONJUNTOISOLADOR("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-od-at-um-ip/detect", "model1"),
	TIPOISOLADORFERRAGEM("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-od-at-um-ip/detect", "model2"),
	VIDROQUEBRADO("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-od-at-um-ip/detect", "model3"),
	POLIMERICOCHAMUSCADO("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-od-at-um-ip/detect", "model4"),
	VIDROPOLUIDO("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-ic-at-um-ip/classify", "model1"),
	POLIMERICOPOLUIDO("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-ic-at-um-ip/classify", "model2"),
	FERRAGEMOXIDADA("https://vsystem.ingress.dh-gdrs57j9.di-us-east.shoot.live.k8s-hana.ondemand.com/app/pipeline-modeler/openapi/service/models-ic-at-um-ip/classify", "model3");
	
	String urlModelo;
	String nomeModelo;
	
	public String getUrlModelo() {
		return urlModelo;
	}
	public String getNomeModelo() {
		return nomeModelo;
	}
	

	
	
}
