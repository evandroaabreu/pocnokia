package com.nokia.poc.pocIsoladores.enumeration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum TipoDefeitoEnum {
	
	VIDROQUEBRADO("Vidro Quebrado"),
	VIDROPOLUIDO("Vidro Poluído"),
	POLIMERICOCHAMUSCADO("Polimérico Chamuscado"),
	POLIMERICOPOLUIDO("Polimérico Poluído"),
	FERRAGEMOXIDADA("Ferragem oxidada");
	
	String defeito;

	public String getDefeitoDescricao() {
		return defeito;
	}
	
}
