package com.nokia.poc.pocIsoladores.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MensagemTransacaoEnum {
	
	
	SUCESSO(0, "Salvo com sucesso"),
	RESULTADORECUPERADOSUCESSO(0, "Resultado recuperado com sucesso"),
	DADOSNAOCADASTRADO(3, "Dados não cadastrado"),
	DADOSDELETADO(0, "Dados deletado com sucesso"),
	ERRO(3,"Erro ao salvar os dados");	

	
	private Integer value;
	private String description;
}
