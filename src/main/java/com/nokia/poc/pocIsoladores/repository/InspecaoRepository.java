package com.nokia.poc.pocIsoladores.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nokia.poc.pocIsoladores.entity.InspecaoResultado;

@Repository
public interface InspecaoRepository  extends JpaRepository<InspecaoResultado, Integer> {
		
	@Query("select i.id from InspecaoResultado i where processado = :processado")
	List<Object> findAllByProcessado(Boolean processado);
	                   

}
