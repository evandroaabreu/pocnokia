package com.nokia.poc.pocIsoladores.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "INSPECAO_RESULTADO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InspecaoResultado {
	
	@Id
	@SequenceGenerator(name = "inspecaoresultado_id", sequenceName = "inspecaoresultado_sequence", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "inspecaoresultado_id")
	private Integer id;
	
	@Column(name="file_url")
	private String fileUrl;
	
	@Lob
	@JsonIgnore
	@Column(name="imagem")
	private byte[] imagem;
	
	@Column(name="latitude")	
	private Double latitude;
	
	@Column(name="longitude")
	private Double longitude;
		
	@Column(length = 120,name="descricao_defeito")
	private String descricaoDefeito;	

	@Column(length = 50, name="tipo_isolador")
	private String tipoIsolador;	
	
	@Column(name="processado", columnDefinition = "boolean default false")
	private Boolean processado;	
	
}
