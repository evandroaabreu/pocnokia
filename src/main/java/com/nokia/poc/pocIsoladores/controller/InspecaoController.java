package com.nokia.poc.pocIsoladores.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nokia.poc.pocIsoladores.dto.InspecaoResultadoDto;
import com.nokia.poc.pocIsoladores.entity.InspecaoResultado;
import com.nokia.poc.pocIsoladores.service.InspecaoService;
import com.nokia.poc.pocIsoladores.service.InspecaoXlsService;
import com.nokia.poc.pocIsoladores.util.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-inspecao")
@Api(value = "API Inspeção")
public class InspecaoController {
	
	@Autowired
	private InspecaoService inspecaoService;

	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "apaga por id", response = String.class)
	public ResponseEntity<Response> apagaPorId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(inspecaoService.apagaPorId(id));
	}	
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna por id", response = String.class)
	public ResponseEntity<InspecaoResultadoDto> getPorId(@PathVariable("id") Integer id) {
		return ResponseEntity.ok(inspecaoService.getPorId(id));
	}	
	
	@GetMapping("")
	@ApiOperation(value = "Retorna todas inspeções", response = String.class)
	public ResponseEntity<List<InspecaoResultado>> getTodos() {
		return ResponseEntity.ok(inspecaoService.getTodos());
	}	
	
	@GetMapping("/getTodosProcessados")
	@ApiOperation(value = "Retorna todas inspeções processadas", response = String.class)
	public ResponseEntity<List<InspecaoResultadoDto>> getTodosProcessados() {
		return ResponseEntity.ok(inspecaoService.getTodosProcessados());
	}	
	
	
	@GetMapping(value = "/getExcelPorId/{id}",produces = "application/vnd.ms-excel")
	@ApiOperation(value = "Retorna formato Excel por id", response = String.class)
	public ResponseEntity<InputStreamResource> getExcelPorId(@PathVariable Integer id) throws IOException {
		return ResponseEntity.ok(inspecaoService.getExcelPorId(id));
	}
	

	
	@GetMapping(value = "/getTodosExcel",produces = "application/vnd.ms-excel")
	@ApiOperation(value = "Retorna todas inspeções no formato Excel", response = String.class)
	public ResponseEntity<InputStreamResource> getTodosExcel() throws IOException {
		return ResponseEntity.ok(inspecaoService.getTodosExcel());		
	}
	
	
	@PostMapping
	@ApiOperation(value = "Salva os campos das inspecao", response = String.class)
	public ResponseEntity<Response> salvaInspecao(
			@RequestParam("foto") MultipartFile foto,
			@RequestParam("latitude") Double latitude,
			@RequestParam("longitude") Double longitude)  throws IllegalArgumentException, IllegalAccessException, Exception{
		return ResponseEntity.ok(inspecaoService.salvaInspecao(foto,latitude,longitude));
	}
	
	@PostMapping(value = "/enviaEmailXls")
	@ApiOperation(value = "Enviar email", response = String.class)
	public ResponseEntity<Response> enviarEmailXls(
			@RequestParam("email") String email)  throws IllegalArgumentException, IllegalAccessException, Exception{
		return ResponseEntity.ok(inspecaoService.enviarEmailXls(email));
	}	

		
	


}
