package com.nokia.poc.pocIsoladores.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nokia.poc.pocIsoladores.service.OrquestradorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api-orquestrador")
@Api(value = "API Orquestrador")
public class OrquestradorController {
	
	@Autowired
	private OrquestradorService orqService;

	@PostMapping("/inferenceMl")
	@ApiOperation(value = "Inferência do ML")
	public ResponseEntity<?> inferenceMl(@RequestParam("imagem") MultipartFile imagem, @RequestParam("idResultado") Integer idResultado) {
		try {
			return ResponseEntity.ok(orqService.inferenceMl(imagem,idResultado));
		} catch (Exception e) {
			return new ResponseEntity<>(
			          "ERROR", 
			          HttpStatus.BAD_REQUEST);
		}
	}
	
}
