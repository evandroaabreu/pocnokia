package com.nokia.poc.pocIsoladores.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nokia.poc.pocIsoladores.dto.InspecaoResultadoDto;
import com.nokia.poc.pocIsoladores.entity.InspecaoResultado;
import com.nokia.poc.pocIsoladores.enumeration.MensagemGeralEnum;
import com.nokia.poc.pocIsoladores.enumeration.MensagemTransacaoEnum;
import com.nokia.poc.pocIsoladores.repository.InspecaoRepository;
import com.nokia.poc.pocIsoladores.util.Response;
import com.nokia.poc.pocIsoladores.util.Util;
import java.nio.file.Files;

@Service
public class InspecaoService {

	@Autowired
	private InspecaoRepository inspecaoRepository;
	
	@Autowired
	private InspecaoXlsService inspecaoXlsService;
	
	@Autowired
	private OrquestradorService orquestradorService;
	
	@Autowired
	private AwsS3Service awsS3Service;

	@Autowired
	private EnvioEmailService envioEmailService;

	
	public Response enviarEmailXls(String email) {
		  return envioEmailService.envioEmail(email) == Boolean.TRUE ? 
				  Util.buildResponse(MensagemGeralEnum.EMAILENVIADOCOMSUCESSO.getValue(),
						  MensagemGeralEnum.EMAILENVIADOCOMSUCESSO.getDescription()): 
							     Util.buildResponse(MensagemGeralEnum.EMAILCOMFALHA.getValue(),
								  MensagemGeralEnum.EMAILCOMFALHA.getDescription());		
	}

	
	public InputStreamResource getTodosExcel() throws IOException {
		return inspecaoXlsService.montarExcel(inspecaoRepository.findAllByProcessado(true));
	}
	
	
	public InputStreamResource getExcelPorId(Integer id) throws IOException {
		return inspecaoXlsService.montarExcelPorId(id);
	}
	
	public Response apagaPorId(Integer id) {
		Optional<InspecaoResultado> inspecaoOpt = inspecaoRepository.findById(id);
		if (!inspecaoOpt.isPresent()) {
			return Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
					MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription());
		}
		
		try {	
			inspecaoRepository.delete(inspecaoOpt.get());
			return Util.buildResponse(MensagemTransacaoEnum.DADOSDELETADO.getValue(),
					MensagemTransacaoEnum.DADOSDELETADO.getDescription());		
		
		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());		
		}

	}	

	public InspecaoResultadoDto getPorId(Integer id) {
		Optional<InspecaoResultado> inspecaoOpt = inspecaoRepository.findById(id);
		if (!inspecaoOpt.isPresent()) {
			return builderInspecaoResultadoDtoIdNaoEncontrado();
		}

		return builderInspecaoResultadoDto(inspecaoOpt.get());
	}

	public List<InspecaoResultado> getTodos() {
		return inspecaoRepository.findAll();

	}
	
	
	public List<InspecaoResultadoDto> getTodosProcessados() {	
		List<Object> lst = inspecaoRepository.findAllByProcessado(true);
		List<InspecaoResultadoDto> lstDto = new ArrayList<InspecaoResultadoDto>();
		lst.forEach(inspecao -> {
			Integer id = (Integer) inspecao;
			Optional<InspecaoResultado> result = inspecaoRepository.findById(id);
			if(result.isPresent()) {
				InspecaoResultado inspecaoResultado = result.get();
				lstDto.add(builderInspecaoResultadoDto(inspecaoResultado));
			}
		});
		return lstDto;
	}

	public Response salvaInspecao(MultipartFile foto, Double latitude, Double longitude) {

		try {
			byte[] byteArray = getFile(foto);
			File fileOrq = Util.convertByteArrayToFileImg(byteArray);
			String s3Url = "";
			try {
				s3Url = awsS3Service.uploadFile(fileOrq);
			} catch (Exception e1) {
				return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
						"Erro ao salvar imagem" + e1);
			}
			InspecaoResultado resultadoSalvo = inspecaoRepository.save(builderInspecaoResultado(null, latitude, longitude,s3Url));
			Integer id = resultadoSalvo.getId();
			new Thread(() -> {
				Integer tentativas = 0;
				Integer maxTries = 2;
				while(true) {
					try {
						orquestradorService.inferenceMlFile(fileOrq, id);
						break;
					} catch (Exception e) {
						if(++tentativas == maxTries) {
							break;
						}
					}
				}
			}).start();
			return Util.buildResponse(MensagemTransacaoEnum.SUCESSO.getValue(),
					MensagemTransacaoEnum.SUCESSO.getDescription());
		} catch (Exception e) {
			return Util.buildResponse(MensagemTransacaoEnum.ERRO.getValue(),
					MensagemTransacaoEnum.ERRO.getDescription());
		}
	}

	private InspecaoResultadoDto builderInspecaoResultadoDtoIdNaoEncontrado() {
		return InspecaoResultadoDto.builder()
				.response(Util.buildResponse(MensagemTransacaoEnum.DADOSNAOCADASTRADO.getValue(),
						MensagemTransacaoEnum.DADOSNAOCADASTRADO.getDescription()))
				.build();
	}

	private InspecaoResultadoDto builderInspecaoResultadoDto(InspecaoResultado inspecaoResultado) {
		return InspecaoResultadoDto.builder().id(inspecaoResultado.getId()).fileUrl(inspecaoResultado.getFileUrl())
				.latitude(inspecaoResultado.getLatitude()).longitude(inspecaoResultado.getLongitude())
				.descricaoDefeito(inspecaoResultado.getDescricaoDefeito())
				.tipoIsolador(inspecaoResultado.getTipoIsolador())
				.imagem(inspecaoResultado.getImagem())
				.response(Util.buildResponse(MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getValue(),
						MensagemTransacaoEnum.RESULTADORECUPERADOSUCESSO.getDescription()))
				.build();
	}

	private InspecaoResultado builderInspecaoResultado(byte[] foto, Double latitude, Double longitude ,String s3Url) throws IllegalStateException, IOException {
		return InspecaoResultado.builder()
				.imagem(foto)
				.fileUrl(s3Url)
				.latitude(latitude).longitude(longitude)
				.processado(Boolean.FALSE).build();
	}

	private String getFileAws(MultipartFile foto) {
		return "c:\teste.jpg";
	}
	
	private byte[] getFile(MultipartFile foto) throws IllegalStateException, IOException {
		Date date = new Date();
		String dataNew = date.getTime() + "";
		File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + dataNew);
		foto.transferTo(convFile);
		byte[] imagemBytes = Files.readAllBytes(convFile.toPath());
		return imagemBytes;
	}
	
}
