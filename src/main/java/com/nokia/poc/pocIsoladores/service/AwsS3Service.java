package com.nokia.poc.pocIsoladores.service;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.nokia.poc.pocIsoladores.dto.CredentialsDTO;
import com.nokia.poc.pocIsoladores.util.Util;

@Service
public class AwsS3Service {
	
	@Autowired
	private CrendentialsService crendentialsService;
	
	final String bucket_name = "poc-nokia-bucket";
	final String urlS3 = "s3.us-west-2.amazonaws.com";
	
	public String uploadFile(File file) throws Exception {
		try {
			AmazonS3 s3 = connectToS3();
			String key = Long.toString(new Date().getTime())+".jpg";
		    s3.putObject(bucket_name, key, file);
		    String url = "https://"+bucket_name+"."+urlS3+"/"+key;
		    return url;
		} catch (Exception e) {
		    throw e;
		}
	}

	private AmazonS3 connectToS3() throws IOException {
		CredentialsDTO credential = crendentialsService.loadCredentialsAWS();
		System.setProperty(SDKGlobalConfiguration.ACCESS_KEY_SYSTEM_PROPERTY, credential.getAccessKey());
		System.setProperty(SDKGlobalConfiguration.SECRET_KEY_SYSTEM_PROPERTY, credential.getSecretKey());
		System.setProperty(SDKGlobalConfiguration.AWS_REGION_SYSTEM_PROPERTY, credential.getRegion());
		AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.DEFAULT_REGION).build();
		return s3;
	}
	
	

}
