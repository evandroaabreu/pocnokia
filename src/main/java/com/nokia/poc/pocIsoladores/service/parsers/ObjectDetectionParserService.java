package com.nokia.poc.pocIsoladores.service.parsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ObjectDetectionParserService {
	
	
	public List<HashMap<String, String>> parseObject(String json) throws JSONException {
		try {
			JSONObject obj = new JSONObject(json);
			List<HashMap<String, String>> retorno = new ArrayList<HashMap<String, String>>();
			JSONArray detectionBoxes = obj.getJSONArray("detection_boxes");
			Integer numberOfDetection = obj.getJSONArray("detection_boxes").length();
			JSONArray detectionClasses = obj.getJSONArray("detection_classes");
			JSONArray detectionScores = obj.getJSONArray("detection_scores");
			
			//Percorre o numero de detecções. Obtem bound boxes, classe e score
			//Exemplo de um elemento da lista:
			//<"Classe","Isolador">
			//<"Score","0.9333635568618774">
			//<"Boxes","0.2392474263906479,0.4402000904083252,0.42100873589515686,0.5239555239677429">
			for(int i = 0 ; i < numberOfDetection ; i++) {
				HashMap<String, String> retornoAux = new HashMap<String, String>();
				retornoAux.put("Classe", detectionClasses.getString(i));
				retornoAux.put("Score",""+detectionScores.getDouble(i));
				JSONArray boxes = detectionBoxes.getJSONArray(i);
				if(boxes.length() >= 1) {
					String boxesString = "" + boxes.getDouble(0);
					for(int j = 1 ; j < boxes.length() ; j++) {
						boxesString = boxesString + "," + boxes.getDouble(j);
					}
					retornoAux.put("Boxes",boxesString);
				}
				retorno.add(retornoAux);
			}
			return retorno;
		} catch (JSONException e) {
			throw new JSONException(e.toString());
		}
	}
	
	public List<HashMap<String, String>> bestAccuracyResult(String retApi) throws JSONException {
		//checar os boxes com accurary > 75%.
		//retornar lista com os boxes que atendem o requisito..
		List<HashMap<String, String>> parsedObject = parseObject(retApi);
		List<HashMap<String, String>> ojectsReturn = new ArrayList<HashMap<String,String>>();
		for(HashMap<String, String> object : parsedObject) {
			if(Double.valueOf(object.get("Score")) > 0.75) {
				ojectsReturn.add(object);
			}
		}
		return ojectsReturn;
	}
	
}
