package com.nokia.poc.pocIsoladores.service;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.nokia.poc.pocIsoladores.util.Util;

@Service
public class EnvioEmailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private InspecaoXlsService inspecaoXlsService;
	
	private static String subject = "POC NOKIA - Listagem ";

	private String getSubject() {

		return "<br><h1>Olá</h1><br><p></p><br> Foi solicitada o envio do relatório por e-mail. <br>" + "<br><br>"
				+ "Favor não responder à este email. Esta é uma mensagem automática.";
	}

	public Boolean envioEmail(String to) {

		try {
			MimeMessage mail = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setFrom("pocNokia@poc.com.br");
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText("<html><body>" + getSubject() + "</body></html>",
					true);
			helper.setCc("equatorial.fitec@gmail.com");
			File file = Util.convertByteArrayToFileXls(inspecaoXlsService.montarExcelEmail());
			helper.addAttachment("inspecao.xls", file);
			
			javaMailSender.send(mail);
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

}
