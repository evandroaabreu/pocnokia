package com.nokia.poc.pocIsoladores.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import com.nokia.poc.pocIsoladores.entity.InspecaoResultado;
import com.nokia.poc.pocIsoladores.repository.InspecaoRepository;

@Service
public class InspecaoXlsService {
	
	@Autowired
	private InspecaoRepository inspecaoRepository;

	private Integer rownum;
	private Integer cellnum;

	private Row row;
	private Integer maxCaracteres = 50;
	private HSSFWorkbook workbook = new HSSFWorkbook();
	private HSSFSheet sheetInspecaoXLS = workbook.createSheet("InspecaoXLS");

	public InputStreamResource montarExcel(List<Object> list) throws IOException {
		try {
			
			ArrayList<InspecaoResultado> listInspecoes = new ArrayList<InspecaoResultado>();
			list.forEach(inspecao -> {
				Integer id = (Integer) inspecao;
				Optional<InspecaoResultado> result = inspecaoRepository.findById(id);
				if(result.isPresent()) {
					InspecaoResultado inspecaoResultado = result.get();
					listInspecoes.add(inspecaoResultado);
				}
			});


			setaTamanhoColuna();

			HSSFCellStyle inspecaoStyleChave = workbook.createCellStyle();
			HSSFFont inspecaoStyleChaveFont = workbook.createFont();
			inspecaoStyleChaveFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleChaveFont.setFontHeightInPoints((short) 10);
			inspecaoStyleChaveFont.setBold(true);
			inspecaoStyleChave.setFont(inspecaoStyleChaveFont);

			HSSFCellStyle inspecaoStyleValor = workbook.createCellStyle();
			HSSFFont inspecaoStyleValorFont = workbook.createFont();
			inspecaoStyleValorFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleValor.setFont(inspecaoStyleValorFont);

			rownum = 0;
			cellnum = 0;
			listInspecoes.forEach(inspecao -> {
				row = sheetInspecaoXLS.createRow(rownum++);
				Cell cellDescricaoDefeito = row.createCell(cellnum++);
				cellDescricaoDefeito.setCellValue("DESCRIÇÃO DO DEFEITO:");
				cellDescricaoDefeito.setCellStyle(inspecaoStyleChave);

				Cell cellDescricaoDefeitoValor = row.createCell(cellnum++);
				cellDescricaoDefeitoValor.setCellValue(inspecao.getDescricaoDefeito());
				cellDescricaoDefeitoValor.setCellStyle(inspecaoStyleValor);

				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell cellTipoIsolador = row.createCell(cellnum++);
				cellTipoIsolador.setCellValue("TIPO DE ISOLADOR:");
				cellTipoIsolador.setCellStyle(inspecaoStyleChave);
				Cell cellTipoIsoladorValor = row.createCell(cellnum++);
				cellTipoIsoladorValor.setCellValue(inspecao.getTipoIsolador());
				cellTipoIsoladorValor.setCellStyle(inspecaoStyleValor);

				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell celllatitude = row.createCell(cellnum++);
				celllatitude.setCellValue("LATITUDE:");
				celllatitude.setCellStyle(inspecaoStyleChave);
				Cell cellLatitudeValor = row.createCell(cellnum++);
				cellLatitudeValor.setCellValue(inspecao.getLatitude());
				cellLatitudeValor.setCellStyle(inspecaoStyleValor);
				
				
				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell celllongitude = row.createCell(cellnum++);
				celllongitude.setCellValue("LONGITUDE:");
				celllongitude.setCellStyle(inspecaoStyleChave);
				Cell cellLongitudeValor = row.createCell(cellnum++);
				cellLongitudeValor.setCellValue(inspecao.getLongitude());
				cellLongitudeValor.setCellStyle(inspecaoStyleValor);				

				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell cellProcessado = row.createCell(cellnum++);
				cellProcessado.setCellValue("PROCESSADO:");
				cellProcessado.setCellStyle(inspecaoStyleChave);
				Cell cellProcessadoValor = row.createCell(cellnum++);
				cellProcessadoValor.setCellValue(inspecao.getProcessado() == Boolean.TRUE ? "Sim" : "Não");
				cellProcessadoValor.setCellStyle(inspecaoStyleValor);

				/*try {
					byte[] bytes = inspecao.getImagem();

					int imagem_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
					///imagem.close();
					HSSFPatriarch drawing = sheetInspecaoXLS.createDrawingPatriarch();
					ClientAnchor anchor = new HSSFClientAnchor();
					anchor.setCol1(0);
					anchor.setRow1(rownum++);
					HSSFPicture picture = drawing.createPicture(anchor, imagem_id);
					picture.resize();
					
					

				} catch (Exception e) {
				}*/

				rownum++;
				cellnum = 0;
			});

			return retornoInputStream();
		} catch (Exception e) {
		}
		return null;

	}

	public InputStreamResource montarExcelPorId(Integer id) {
		try {
			
			Optional<InspecaoResultado> inspecaoOpt = inspecaoRepository.findById(id);
			if (!inspecaoOpt.isPresent()) {
				return null;
			}

			setaTamanhoColuna();

			HSSFCellStyle inspecaoStyleChave = workbook.createCellStyle();
			HSSFFont inspecaoStyleChaveFont = workbook.createFont();
			inspecaoStyleChaveFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleChaveFont.setFontHeightInPoints((short) 10);
			inspecaoStyleChaveFont.setBold(true);
			inspecaoStyleChave.setFont(inspecaoStyleChaveFont);

			HSSFCellStyle inspecaoStyleValor = workbook.createCellStyle();
			HSSFFont inspecaoStyleValorFont = workbook.createFont();
			inspecaoStyleValorFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleValor.setFont(inspecaoStyleValorFont);

			row = sheetInspecaoXLS.createRow(0);
			cellnum = 0;
			Cell cellDescricaoDefeito = row.createCell(cellnum++);
			cellDescricaoDefeito.setCellValue("DESCRIÇÃO DO DEFEITO:");
			cellDescricaoDefeito.setCellStyle(inspecaoStyleChave);

			Cell cellDescricaoDefeitoValor = row.createCell(cellnum++);
			cellDescricaoDefeitoValor.setCellValue(inspecaoOpt.get().getDescricaoDefeito());
			cellDescricaoDefeitoValor.setCellStyle(inspecaoStyleValor);

			row = sheetInspecaoXLS.createRow(1);
			cellnum = 0;
			Cell cellTipoIsolador = row.createCell(cellnum++);
			cellTipoIsolador.setCellValue("TIPO DE ISOLADOR:");
			cellTipoIsolador.setCellStyle(inspecaoStyleChave);
			Cell cellTipoIsoladorValor = row.createCell(cellnum++);
			cellTipoIsoladorValor.setCellValue(inspecaoOpt.get().getTipoIsolador());
			cellTipoIsoladorValor.setCellStyle(inspecaoStyleValor);

			row = sheetInspecaoXLS.createRow(2);
			cellnum = 0;
			Cell celllatitude = row.createCell(cellnum++);
			celllatitude.setCellValue("LATITUDE:");
			celllatitude.setCellStyle(inspecaoStyleChave);
			Cell cellLatitudeValor = row.createCell(cellnum++);
			cellLatitudeValor.setCellValue(inspecaoOpt.get().getLatitude());
			cellLatitudeValor.setCellStyle(inspecaoStyleValor);
			
			row = sheetInspecaoXLS.createRow(3);
			cellnum = 0;
			Cell celllongitude = row.createCell(cellnum++);
			celllongitude.setCellValue("LONGITUDE:");
			celllongitude.setCellStyle(inspecaoStyleChave);
			Cell cellLongitudeValor = row.createCell(cellnum++);
			cellLongitudeValor.setCellValue(inspecaoOpt.get().getLongitude());
			cellLongitudeValor.setCellStyle(inspecaoStyleValor);				
			

			row = sheetInspecaoXLS.createRow(4);
			cellnum = 0;
			Cell cellProcessado = row.createCell(cellnum++);
			cellProcessado.setCellValue("PROCESSADO:");
			cellProcessado.setCellStyle(inspecaoStyleChave);
			Cell cellProcessadoValor = row.createCell(cellnum++);
			cellProcessadoValor.setCellValue(inspecaoOpt.get().getProcessado() == Boolean.TRUE ? "Sim" : "Não");
			cellProcessadoValor.setCellStyle(inspecaoStyleValor);

			///try {
				///InputStream imagem = new FileInputStream("C:/fitec/imagens/2.jpg");
				///byte[] bytes = IOUtils.toByteArray(imagem);
			///	byte[] bytes = inspecaoOpt.get().getImagem();

				///int imagem_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
				////imagem.close();
				///HSSFPatriarch drawing = sheetInspecaoXLS.createDrawingPatriarch();
				///ClientAnchor anchor = new HSSFClientAnchor();
				///anchor.setCol1(0);
				///anchor.setRow1(4);
				//HSSFPicture picture = drawing.createPicture(anchor, imagem_id);
				//picture.resize();

			//} catch (IOException e) {
			///}
			return retornoInputStream();
		} catch (Exception e) {
		}
		return null;
	}

	private void setaTamanhoColuna() {
		int width = ((int) (maxCaracteres * 1.14388)) * 256;
		sheetInspecaoXLS.setColumnWidth(0, width);
		sheetInspecaoXLS.setColumnWidth(1, width);
	}

	private InputStreamResource retornoInputStream() throws IOException {
		ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
		workbook.write(fileOut);
		workbook.close();

		byte[] fileByteArray = fileOut.toByteArray();

		return new InputStreamResource(new ByteArrayInputStream(fileByteArray));
	}
	
	public byte[] montarExcelPorIdEmail(Integer id) {
		try {
			
			Optional<InspecaoResultado> inspecaoOpt = inspecaoRepository.findById(id);
			if (!inspecaoOpt.isPresent()) {
				return null;
			}

			setaTamanhoColuna();

			HSSFCellStyle inspecaoStyleChave = workbook.createCellStyle();
			HSSFFont inspecaoStyleChaveFont = workbook.createFont();
			inspecaoStyleChaveFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleChaveFont.setFontHeightInPoints((short) 10);
			inspecaoStyleChaveFont.setBold(true);
			inspecaoStyleChave.setFont(inspecaoStyleChaveFont);

			HSSFCellStyle inspecaoStyleValor = workbook.createCellStyle();
			HSSFFont inspecaoStyleValorFont = workbook.createFont();
			inspecaoStyleValorFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleValor.setFont(inspecaoStyleValorFont);

			row = sheetInspecaoXLS.createRow(0);
			cellnum = 0;
			Cell cellDescricaoDefeito = row.createCell(cellnum++);
			cellDescricaoDefeito.setCellValue("DESCRIÇÃO DO DEFEITO:");
			cellDescricaoDefeito.setCellStyle(inspecaoStyleChave);

			Cell cellDescricaoDefeitoValor = row.createCell(cellnum++);
			cellDescricaoDefeitoValor.setCellValue(inspecaoOpt.get().getDescricaoDefeito());
			cellDescricaoDefeitoValor.setCellStyle(inspecaoStyleValor);

			row = sheetInspecaoXLS.createRow(1);
			cellnum = 0;
			Cell cellTipoIsolador = row.createCell(cellnum++);
			cellTipoIsolador.setCellValue("TIPO DE ISOLADOR:");
			cellTipoIsolador.setCellStyle(inspecaoStyleChave);
			Cell cellTipoIsoladorValor = row.createCell(cellnum++);
			cellTipoIsoladorValor.setCellValue(inspecaoOpt.get().getTipoIsolador());
			cellTipoIsoladorValor.setCellStyle(inspecaoStyleValor);

			row = sheetInspecaoXLS.createRow(2);
			cellnum = 0;
			Cell celllatitude = row.createCell(cellnum++);
			celllatitude.setCellValue("LATITUDE:");
			celllatitude.setCellStyle(inspecaoStyleChave);
			Cell cellLatitudeValor = row.createCell(cellnum++);
			cellLatitudeValor.setCellValue(inspecaoOpt.get().getLatitude());
			cellLatitudeValor.setCellStyle(inspecaoStyleValor);
			
			row = sheetInspecaoXLS.createRow(3);
			cellnum = 0;
			Cell celllongitude = row.createCell(cellnum++);
			celllongitude.setCellValue("LONGITUDE:");
			celllongitude.setCellStyle(inspecaoStyleChave);
			Cell cellLongitudeValor = row.createCell(cellnum++);
			cellLongitudeValor.setCellValue(inspecaoOpt.get().getLongitude());
			cellLongitudeValor.setCellStyle(inspecaoStyleValor);				
			

			row = sheetInspecaoXLS.createRow(4);
			cellnum = 0;
			Cell cellProcessado = row.createCell(cellnum++);
			cellProcessado.setCellValue("PROCESSADO:");
			cellProcessado.setCellStyle(inspecaoStyleChave);
			Cell cellProcessadoValor = row.createCell(cellnum++);
			cellProcessadoValor.setCellValue(inspecaoOpt.get().getProcessado() == Boolean.TRUE ? "Sim" : "Não");
			cellProcessadoValor.setCellStyle(inspecaoStyleValor);
			
			ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
			workbook.write(fileOut);
			workbook.close();

			byte[] fileByteArray = fileOut.toByteArray();
			

			return fileByteArray;
		} catch (Exception e) {
		}
		return null;
	}

	public byte[] montarExcelEmail() throws IOException {
		try {
			
			ArrayList<InspecaoResultado> listInspecoes = new ArrayList<InspecaoResultado>();
			
			List<Object> list = inspecaoRepository.findAllByProcessado(true);
			
			list.forEach(inspecao -> {
				Integer id = (Integer) inspecao;
				Optional<InspecaoResultado> result = inspecaoRepository.findById(id);
				if(result.isPresent()) {
					InspecaoResultado inspecaoResultado = result.get();
					listInspecoes.add(inspecaoResultado);
				}
			});


			setaTamanhoColuna();

			HSSFCellStyle inspecaoStyleChave = workbook.createCellStyle();
			HSSFFont inspecaoStyleChaveFont = workbook.createFont();
			inspecaoStyleChaveFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleChaveFont.setFontHeightInPoints((short) 10);
			inspecaoStyleChaveFont.setBold(true);
			inspecaoStyleChave.setFont(inspecaoStyleChaveFont);

			HSSFCellStyle inspecaoStyleValor = workbook.createCellStyle();
			HSSFFont inspecaoStyleValorFont = workbook.createFont();
			inspecaoStyleValorFont.setFontName(HSSFFont.FONT_ARIAL);
			inspecaoStyleValor.setFont(inspecaoStyleValorFont);

			rownum = 0;
			cellnum = 0;
			listInspecoes.forEach(inspecao -> {
				row = sheetInspecaoXLS.createRow(rownum++);
				Cell cellDescricaoDefeito = row.createCell(cellnum++);
				cellDescricaoDefeito.setCellValue("DESCRIÇÃO DO DEFEITO:");
				cellDescricaoDefeito.setCellStyle(inspecaoStyleChave);

				Cell cellDescricaoDefeitoValor = row.createCell(cellnum++);
				cellDescricaoDefeitoValor.setCellValue(inspecao.getDescricaoDefeito());
				cellDescricaoDefeitoValor.setCellStyle(inspecaoStyleValor);

				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell cellTipoIsolador = row.createCell(cellnum++);
				cellTipoIsolador.setCellValue("TIPO DE ISOLADOR:");
				cellTipoIsolador.setCellStyle(inspecaoStyleChave);
				Cell cellTipoIsoladorValor = row.createCell(cellnum++);
				cellTipoIsoladorValor.setCellValue(inspecao.getTipoIsolador());
				cellTipoIsoladorValor.setCellStyle(inspecaoStyleValor);

				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell celllatitude = row.createCell(cellnum++);
				celllatitude.setCellValue("LATITUDE:");
				celllatitude.setCellStyle(inspecaoStyleChave);
				Cell cellLatitudeValor = row.createCell(cellnum++);
				cellLatitudeValor.setCellValue(inspecao.getLatitude());
				cellLatitudeValor.setCellStyle(inspecaoStyleValor);
				
				
				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell celllongitude = row.createCell(cellnum++);
				celllongitude.setCellValue("LONGITUDE:");
				celllongitude.setCellStyle(inspecaoStyleChave);
				Cell cellLongitudeValor = row.createCell(cellnum++);
				cellLongitudeValor.setCellValue(inspecao.getLongitude());
				cellLongitudeValor.setCellStyle(inspecaoStyleValor);				

				row = sheetInspecaoXLS.createRow(rownum++);
				cellnum = 0;
				Cell cellProcessado = row.createCell(cellnum++);
				cellProcessado.setCellValue("PROCESSADO:");
				cellProcessado.setCellStyle(inspecaoStyleChave);
				Cell cellProcessadoValor = row.createCell(cellnum++);
				cellProcessadoValor.setCellValue(inspecao.getProcessado() == Boolean.TRUE ? "Sim" : "Não");
				cellProcessadoValor.setCellStyle(inspecaoStyleValor);

				rownum++;
				cellnum = 0;
			});

			ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
			workbook.write(fileOut);
			workbook.close();

			byte[] fileByteArray = fileOut.toByteArray();
			

			return fileByteArray;

		} catch (Exception e) {
		}
		return null;

	}

	
	

}
