package com.nokia.poc.pocIsoladores.service;
	
	import java.io.ByteArrayInputStream;
	import java.io.File;
	import java.io.IOException;
	import java.io.InputStream;
	import java.util.Arrays;
	import java.util.HashMap;
	import org.springframework.core.io.FileSystemResource;
	import org.springframework.http.HttpEntity;
	import org.springframework.http.HttpHeaders;
	import org.springframework.http.HttpMethod;
	import org.springframework.http.MediaType;
	import org.springframework.http.ResponseEntity;
	import org.springframework.stereotype.Service;
	import org.springframework.util.LinkedMultiValueMap;
	import org.springframework.util.MultiValueMap;
	import org.springframework.web.client.RestTemplate;

	@Service
	public class CallsService {
		
		public static final String BASICAUTH = "Basic ZGVmYXVsdFxhZG1zYXA6QWRtU2FwIzEyMw==";
		public static final String URLTOKEN = "https://equatorial.authentication.us10.hana.ondemand.com/oauth/token?grant_type=client_credentials";
		public static final String CLIENTEID = "sb-503825b0-64f5-4c6d-9c1c-6d3d38d97e91!b1687|foundation-std-mlfpreproduction!b164";
		public static final String CLEINTSECRETS = "RUXePY5a/s/osk8TnmJsldz/sCw=";	
		

		public static MultiValueMap<String, Object> createGenericArgs() {
			MultiValueMap<String, Object> args = new LinkedMultiValueMap<>();	    
		    args.add("Content-Disposition", "form-data");
		    args.add("Content-Type", "application/octet-stream");
		    return args;
		}

		public static HashMap<?, ?> postAPI(MultiValueMap<String, Object> args, HttpHeaders headers, String urlob) {
			HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(args, headers);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<HashMap> response = restTemplate.exchange(urlob,HttpMethod.POST, entity,HashMap.class);
			return checkResponseStatus(response);
		}

		private static HashMap<?, ?> checkResponseStatus(ResponseEntity<HashMap> response) {
			if((""+response.getStatusCodeValue()).charAt(0)=='2') { //Status OK
				return response.getBody();
			}
			else {
				System.out.println("Erro "+response.getStatusCodeValue()+": "+response.getStatusCode().getReasonPhrase());
				return new HashMap<String, Object>();
			}
		}
		
		public static HttpHeaders createHeadersApiPython(){
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			return headers; 
		}
		
		public InputStream callApiPython(File is, Double top, Double left, Double bot, Double right, String urlPython){
			HttpHeaders headers = createHeadersApiPython();
			MultiValueMap<String, Object> args = createGenericArgs();		
		    args.add("image2",new FileSystemResource(is.getAbsolutePath()));
		    args.add("top", top);
		    args.add("left", left);
		    args.add("bot", bot);
		    args.add("right", right);
			HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(args, headers);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<byte[]> response = restTemplate.exchange(urlPython,HttpMethod.POST, entity,byte[].class);
			if((""+response.getStatusCodeValue()).charAt(0)=='2') { //Status OK
				byte[] responseBytes = response.getBody();
				return new ByteArrayInputStream(responseBytes);
			}
			return new InputStream() {
				
				@Override
				public int read() throws IOException {
					return 0;
				}
			};
		}

		/**
		 * Call api python.
		 * Chamada da API do Python para realização do CROP das imagens
		 *
		 * @param image2 the image 2
		 * @param top the top
		 * @param left the left
		 * @param bot the bot
		 * @param right the right
		 * @return the file
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		public InputStream callApiPython(File image, Double top, Double left, Double bot, Double right) throws IOException {
			return callApiPython(image, top, left, bot, right, "https://cropresizepython.cfapps.us20.hana.ondemand.com/api/crop");
		}

		protected String getServicoByName(String nameServico) {
			final String OBJECT_DETECTION = "Object_Detection";
			final String IMAGE_CLASSIFIER_TIPO_ISOLADOR = "Image_Classifier";
			if(nameServico.toLowerCase().contains("obj")
			|| nameServico.toLowerCase().contains("objdet")
			|| nameServico.toLowerCase().contains("Object_Detection")) {
				nameServico = OBJECT_DETECTION;
			}
			if(nameServico.toLowerCase().contains("image")
			|| nameServico.toLowerCase().contains("image_classifier")) {
				nameServico = IMAGE_CLASSIFIER_TIPO_ISOLADOR;
			}
			return nameServico;
		}
		
		public HashMap<?, ?> callDataIntelligence(File is, String url, String modelName) {
			HashMap<?, ?> t = new HashMap<Object, Object>();
			try {
				MultiValueMap<String, Object> args = createGenericArgs();
				args.add("image", new FileSystemResource(is.getAbsolutePath()));
				args.add("model_name", modelName);
				HttpHeaders headers = new HttpHeaders();
				
				headers.add("Authorization", BASICAUTH);
				headers.add("x-requested-with", "Fetch");
				headers.add("cache-control", "no-cache");
				t = postAPI(args, headers, url);
			} catch (Exception e) {
				String msg = "Erro DI Sap" + e.getMessage();
				msg = msg.length() > 200 ? msg.substring(0,199) : msg;
				e.printStackTrace();
			}
		    return t;
		}

}
