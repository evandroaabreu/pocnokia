package com.nokia.poc.pocIsoladores.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nokia.poc.pocIsoladores.dto.ImagensRecortadasDTO;
import com.nokia.poc.pocIsoladores.entity.InspecaoResultado;
import com.nokia.poc.pocIsoladores.repository.InspecaoRepository;
import com.nokia.poc.pocIsoladores.util.Util;

import com.google.common.base.CaseFormat;

@Service
public class OrquestradorService {
	
	@Autowired
	private FlowIsoladorService flowIsolador;
	
	@Autowired
	private FlowDefeitosService flowDefeitos;
	
	@Autowired
	private InspecaoRepository inspecaoRepository;
	
	public List<String> inferenceMl(MultipartFile imagem, Integer idResultado) throws Exception {
		try {
			List<String> defeitos = new ArrayList<String>();
			File file = Util.convertMultiPartFileToFile(imagem);
			List<ImagensRecortadasDTO> imagensProcessadas = flowIsolador.processarImagem(file);
			List<String> tipoIsoladores = new ArrayList<String>();
			processaTipoIsoladores(imagensProcessadas, tipoIsoladores);
			for (ImagensRecortadasDTO imagemRecortada : imagensProcessadas) {
				try {
					processaDefeitos(defeitos, imagemRecortada);
				}
				catch(Exception e) {
					throw e;
				}
			}
			if(idResultado != null) {
				salvaResultados(idResultado, defeitos, tipoIsoladores);	
			}
			return defeitos;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public List<String> inferenceMlFile(File imagem, Integer idResultado) throws Exception {
		try {
			List<String> defeitos = new ArrayList<String>();
			List<ImagensRecortadasDTO> imagensProcessadas = flowIsolador.processarImagem(imagem);
			List<String> tipoIsoladores = new ArrayList<String>();
			processaTipoIsoladores(imagensProcessadas, tipoIsoladores);
			for (ImagensRecortadasDTO imagemRecortada : imagensProcessadas) {
				try {
					processaDefeitos(defeitos, imagemRecortada);
				}
				catch(Exception e) {
					throw e;
				}
			}
			if(idResultado != null) {
				salvaResultados(idResultado, defeitos, tipoIsoladores);	
			}
			return defeitos;
		} catch (Exception e) {
			throw e;
		}
	}

	private void processaDefeitos(List<String> defeitos, ImagensRecortadasDTO imagemRecortada)
			throws IOException, JSONException {
		List<String> listaDefeitos = flowDefeitos.processaDefeitosImagem(imagemRecortada.getImagem(), imagemRecortada.getClasse());
		for (String string : listaDefeitos) {
			if(!defeitos.contains(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, string)))
				defeitos.add(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, string));
		}
	}

	private void processaTipoIsoladores(List<ImagensRecortadasDTO> imagensProcessadas, List<String> tipoIsoladores) {
		imagensProcessadas.forEach(item -> {
			if(!tipoIsoladores.contains(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, item.getClasse()))) {
				tipoIsoladores.add(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, item.getClasse()));
			}
		});
	}

	private void salvaResultados(Integer idResultado, List<String> defeitos, List<String> tipoIsoladores) throws Exception {
		Optional<InspecaoResultado> inspecaoResultado = inspecaoRepository.findById(idResultado);
		if(inspecaoResultado.isPresent()) {
			InspecaoResultado resultado = inspecaoResultado.get();
			resultado.setDescricaoDefeito(Util.convertArrayToStringJson(defeitos));
			resultado.setTipoIsolador(Util.convertArrayToStringJson(tipoIsoladores));
			resultado.setProcessado(true);
			inspecaoRepository.save(resultado);
		}
		else {
			throw new Exception();
		}
	}

}
