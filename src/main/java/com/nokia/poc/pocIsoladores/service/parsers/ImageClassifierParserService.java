package com.nokia.poc.pocIsoladores.service.parsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ImageClassifierParserService {
	
	//Retorna Lista com Hashmaps: <Objeto,Score>
	public List<HashMap<String, String>> parseObject(String json) throws JSONException {
		try {
			JSONObject obj = new JSONObject(json);
			List<HashMap<String, String>> retorno = new ArrayList<HashMap<String, String>>();
			JSONObject outputs = obj.getJSONObject("outputs");
			
			JSONArray labels = outputs.getJSONArray("labels").getJSONArray(0);
			JSONArray probabilities = outputs.getJSONArray("probabilities").getJSONArray(0);
			//Percorre o numero de predições. Obtem classe e score
			//Exemplo de um elemento da lista:
			//<"Petri dish","0.019399">
			for(int i = 0 ; i < labels.length() ; i++) {
				HashMap<String, String> retornoAux = new HashMap<String, String>();
				String label = labels.getString(i);
				Double score = probabilities.getDouble(i);
				retornoAux.put(label,""+score);
				retorno.add(retornoAux);
			}
			return retorno;
		} catch (JSONException e) {
			throw new JSONException(e.toString());

		}
	}
	
	public String bestAccuracyResult(String retApi) throws JSONException {
		List<HashMap<String, String>> parsedObject = parseObject(retApi);
		String ojectReturn = new String();
		Double highestAcc = 0.0;
		for(HashMap<String, String> object : parsedObject) {
			String nomeObjeto = object.keySet().iterator().next();
			Double accuracy = Double.valueOf(object.get(nomeObjeto));
			if(accuracy > highestAcc) {
				highestAcc = accuracy;
				ojectReturn = nomeObjeto;
			}
		}
		//checar o maior accurary..
		return ojectReturn;
	}
}
