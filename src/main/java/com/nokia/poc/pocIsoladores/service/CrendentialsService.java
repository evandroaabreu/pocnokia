package com.nokia.poc.pocIsoladores.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.nokia.poc.pocIsoladores.dto.CredentialsDTO;


@Service
public class CrendentialsService {
	
	@Autowired
    private Environment env;
	
	public CredentialsDTO loadCredentialsAWS() throws IOException {
		CredentialsDTO credential = new CredentialsDTO();
		credential.setAccessKey(env.getProperty("aws-accessKeyId"));
		credential.setSecretKey(env.getProperty("aws-secretKey"));
		credential.setRegion(env.getProperty("aws-region"));
		return credential;
	}

}
