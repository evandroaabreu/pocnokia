package com.nokia.poc.pocIsoladores.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import com.nokia.poc.pocIsoladores.enumeration.ClasseComponenteEnum;
import com.nokia.poc.pocIsoladores.enumeration.ModelosIAEnum;
import com.nokia.poc.pocIsoladores.enumeration.TipoDefeitoEnum;
import com.nokia.poc.pocIsoladores.service.parsers.ImageClassifierParserService;
import com.nokia.poc.pocIsoladores.service.parsers.ObjectDetectionParserService;
import com.nokia.poc.pocIsoladores.util.Util;

@Service
public class FlowDefeitosService {
	
	@Autowired
	private ImageClassifierParserService imageClassifierParser;
	
	@Autowired
	private ObjectDetectionParserService objDetcParser;
	
	@Autowired
	private CallsService callsService;
	
	public List<String> processaDefeitosImagem(InputStream imagem, String classe) throws IOException, JSONException{
		List<String> listaDefeitos = new ArrayList<String>();
		File imgFile = Util.convertImgToFile(imagem);
		if(classe.equalsIgnoreCase(ClasseComponenteEnum.POLIMERICO.getClasse())) {
			if(callOdModel(imgFile,ModelosIAEnum.POLIMERICOCHAMUSCADO))
				listaDefeitos.add(TipoDefeitoEnum.POLIMERICOCHAMUSCADO.getDefeitoDescricao());
			if(callIcModel(imgFile,ModelosIAEnum.POLIMERICOPOLUIDO))
				listaDefeitos.add(TipoDefeitoEnum.POLIMERICOPOLUIDO.getDefeitoDescricao());
		}
		else if(classe.equalsIgnoreCase(ClasseComponenteEnum.VIDRO.getClasse())) {
			if(callOdModel(imgFile,ModelosIAEnum.VIDROQUEBRADO))
				listaDefeitos.add(TipoDefeitoEnum.VIDROQUEBRADO.getDefeitoDescricao());
			if(callIcModel(imgFile,ModelosIAEnum.VIDROPOLUIDO))
				listaDefeitos.add(TipoDefeitoEnum.VIDROPOLUIDO.getDefeitoDescricao());
		}
		else if(classe.equalsIgnoreCase(ClasseComponenteEnum.FERRAGEM.getClasse())) {
			if(callIcModel(imgFile,ModelosIAEnum.FERRAGEMOXIDADA))
				listaDefeitos.add(TipoDefeitoEnum.FERRAGEMOXIDADA.getDefeitoDescricao());
		}
		return listaDefeitos;
	}
	
	private Boolean callOdModel(File imagem, ModelosIAEnum modelo) throws JSONException {
		HashMap<?, ?> callDataIntelligence = callsService.callDataIntelligence(imagem, modelo.getUrlModelo(), modelo.getNomeModelo());
		return checaDefeitoObjDetection(callDataIntelligence);
	}
	
	private Boolean callIcModel(File imagem, ModelosIAEnum modelo) throws JSONException {
		HashMap<?, ?> callDataIntelligence = callsService.callDataIntelligence(imagem, modelo.getUrlModelo(), modelo.getNomeModelo());
		return checaDefeitoImgClassifier(callDataIntelligence);
	}
	
	private boolean checaDefeitoImgClassifier(HashMap<?, ?> retApi) throws JSONException {
		String stringRetApi = new JSONObject(retApi).toString();
		String resultadoDefeito = imageClassifierParser.bestAccuracyResult(stringRetApi);
		//Label poluido ou com_defeito = possui defeito
		if(resultadoDefeito.equalsIgnoreCase("poluido") ||
				resultadoDefeito.equalsIgnoreCase("Ferragem_Oxidada") ||
				resultadoDefeito.equalsIgnoreCase("com_defeito")) {
			return true;
		}
		else {
			return false;
		}
	}

	private boolean checaDefeitoObjDetection(HashMap<?, ?> retApi) throws JSONException {
		if(retApi.isEmpty() || retApi.toString().equals("{}") ||
		   retApi.get(retApi.keySet().iterator().next().toString()).toString().contains("[]")) { 
			//Se não foi encontrado nenhum defeito
			return false;
		}
		else {
			String stringRetApi = new JSONObject(retApi).toString();
			List<HashMap<String, String>> bestAccuracyResult = objDetcParser.bestAccuracyResult(stringRetApi);
			//Se foi encontrado defeito > 75% de acurácia retorna true
			//Se foi encontrado defeito <= 75% de acurácia retorna false
			return !bestAccuracyResult.isEmpty();
		}
	}

}
