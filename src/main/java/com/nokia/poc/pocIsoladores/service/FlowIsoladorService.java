package com.nokia.poc.pocIsoladores.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import com.nokia.poc.pocIsoladores.dto.ImagensRecortadasDTO;
import com.nokia.poc.pocIsoladores.enumeration.ModelosIAEnum;
import com.nokia.poc.pocIsoladores.service.parsers.ObjectDetectionParserService;
import com.nokia.poc.pocIsoladores.util.Util;

@Service
public class FlowIsoladorService {
	
	private List<InputStream> imagensPosOBD = new ArrayList<>();
	
	private List<ImagensRecortadasDTO> imagensProcessadas = new ArrayList<>();
	
			
	@Autowired
	private ObjectDetectionParserService objDetcParser;
	
	@Autowired
	private CallsService callsService;
	
	protected List<ImagensRecortadasDTO> processarImagem(File imagem) throws Exception{
		Integer count = 0;
		Integer maxTries = 3;
		while(true) {
			try {
				imagensPosOBD = new ArrayList<>();
				imagensProcessadas = new ArrayList<>();
				identificacaoConjuntoIsolador(imagem);
				percorrerImagensConjuntoIsolador(imagem);
				return imagensProcessadas;
			} catch (Exception e) {
				if (++count == maxTries) throw e;
			}
		}
	}
	
	private List<InputStream> identificacaoConjuntoIsolador(File imagem) throws Exception {
		HashMap<?, ?> retApi;
		List<InputStream> boundingBoxes = new ArrayList<>();
		try {
			retApi = callsService.callDataIntelligence(imagem,ModelosIAEnum.CONJUNTOISOLADOR.getUrlModelo(), ModelosIAEnum.CONJUNTOISOLADOR.getNomeModelo());
			JSONObject crunchifyObject = new JSONObject(retApi);
			String stringRetApi = crunchifyObject.toString();
			List<HashMap<String, String>> bestAccuracyResult = objDetcParser.bestAccuracyResult(stringRetApi);
			if (!bestAccuracyResult.isEmpty()) {
				boundingBoxes = extrairBoundingBoxes(imagem, bestAccuracyResult);
				imagensPosOBD.addAll(boundingBoxes);
			}
			return boundingBoxes;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void percorrerImagensConjuntoIsolador(File imagem) throws Exception {
		for (InputStream img : imagensPosOBD) {
			try {
				identificacaoTipoIsoladorFerragem(imagem,img);
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	private void identificacaoTipoIsoladorFerragem(File imagem, InputStream img) throws Exception {
		String stringRetApi = "";
		try {
			File imgFile = Util.convertImgToFile(img);
			stringRetApi = callObdTipoIsoladorFerragem(imgFile);
			List<HashMap<String, String>> bestAccuracyResult = objDetcParser.bestAccuracyResult(stringRetApi);
			if (!bestAccuracyResult.isEmpty()) {
				for (HashMap<String, String> hashMap : bestAccuracyResult) {
					ArrayList<HashMap<String, String>> detection = new ArrayList<>();
					detection.add(hashMap);
					InputStream imagemRecorte = extrairBoundingBoxes(imagem, detection).get(0);
					String classe = hashMap.get("Classe"); //Polimerico ou Vidro
					imagensProcessadas.add(new ImagensRecortadasDTO(imagemRecorte, classe));
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<InputStream> extrairBoundingBoxes(File imagem, List<HashMap<String, String>> bestAccuracyResult)
			throws IOException {
		List<InputStream> returnInputStream = new ArrayList<InputStream>();
		// Exemplo:
		// [{Score=0.974321722984314, Classe= isolador,
		// Boxes=0.15674647688865662,0.3235965371131897,0.8400574922561646,0.587823212146759}]
		for (HashMap<String, String> resultObjDet : bestAccuracyResult) {
			// "Parse" dos boxes a serem passados para a API do Python
			String boxes = resultObjDet.get("Boxes");
			String[] boxSplit = boxes.split(",");
			if (boxSplit.length == 4) { // checa se possui todas as coordenadas para fazer crop da imagem
				Double top = Double.valueOf(boxSplit[0]);
				Double left = Double.valueOf(boxSplit[1]);
				Double bot = Double.valueOf(boxSplit[2]);
				Double right = Double.valueOf(boxSplit[3]);
				InputStream callApiPython = callsService.callApiPython(imagem, top, left, bot, right);
				returnInputStream.add(callApiPython);
			}
		}
		return returnInputStream;
	}
	
	protected String callObdTipoIsoladorFerragem(File imgFile) {
		String stringRetApi;
		HashMap<?, ?> retApi;
		retApi = callsService.callDataIntelligence(imgFile, ModelosIAEnum.TIPOISOLADORFERRAGEM.getUrlModelo(), ModelosIAEnum.TIPOISOLADORFERRAGEM.getNomeModelo());
		JSONObject crunchifyObject = new JSONObject(retApi);
		stringRetApi = crunchifyObject.toString();
		return stringRetApi;
	}

}
