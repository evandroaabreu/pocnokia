package com.nokia.poc.pocIsoladores.dto;

import java.io.InputStream;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ImagensRecortadasDTO {
	
	InputStream imagem;
	
	String classe;
	
}
