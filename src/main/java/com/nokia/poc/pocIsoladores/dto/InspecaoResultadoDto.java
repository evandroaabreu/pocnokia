package com.nokia.poc.pocIsoladores.dto;

import com.nokia.poc.pocIsoladores.util.Response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InspecaoResultadoDto {
	
	
	private Integer id;
	
	private String fileUrl;
	
	private Double latitude;
	
	private byte[] imagem;
	
	private Double longitude;
	
	private String descricaoDefeito;
	
	private String tipoIsolador;
	
	private Response response;
	
	

}
