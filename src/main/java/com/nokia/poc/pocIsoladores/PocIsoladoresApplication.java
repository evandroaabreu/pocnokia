package com.nokia.poc.pocIsoladores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocIsoladoresApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(PocIsoladoresApplication.class, args);
	}

}
